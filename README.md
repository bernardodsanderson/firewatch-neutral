# Firewatch Neutral Theme
### For Visual Studio Code

![screenshot](http://i.imgur.com/mR6J1n2.png)

### For more information
* [Gitlab](https://gitlab.com/bernardodsanderson/firewatch-neutral)

**Enjoy!**